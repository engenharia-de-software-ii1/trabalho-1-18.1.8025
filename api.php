<?php

    function veriSoma($x, $y, $z){

        if( (($x + $y) + $z == $x + ($y + $z)) &&
            (($x + $y) == ($y + $x)) &&
            (($x + 0) == $x) && (($y + 0) == $y) &&
            (($x + (- $x)) == 0) && (($y + (- $y)) == 0)   
        ){
            return true;
        }

        return false;
    }

    function veriProduto($x, $y, $z){

        if( (($x * $y) * $z == $x * ($y * $z)) &&
            (($x * $y) == ($y * $x)) &&
            (($x * 1) == $x) && (($y * 1) == $y) &&
            ($x * pow($x, -1) == 1) && ($y * pow($y, -1) == 1)  
        ){
            return true;
        }

        return false;
    }

    function soma($x, $y){
        $z = null;
        
        try {
            if(!(is_numeric($x) && is_numeric($y)))
                throw new Exception('Informe apenas valores númericos');
            $z = $x + $y;
        } catch (\Exception  $e) {
            echo "<script>console.log('Erro: " . $e . "' );</script>";
            return null;
        }        

        if(veriSoma($x, $y, $z))
            return $z;

        return null;
    }

    function produto($x, $y){
        $z = null;
        
        try {
            if(!(is_numeric($x) && is_numeric($y)))
                throw new Exception('Informe apenas valores númericos');
            $z = $x * $y;
        } catch (\Exception  $e) {
            echo "<script>console.log('Erro: " . $e . "' );</script>";
            return null;
        }      

        if(veriProduto($x, $y, $z))
            return $z;

        return null;
    }

    $x = $_POST["x"];
    $y = $_POST["y"];

    $resposta = [
                "soma" => soma($x, $y),
                "produto" => produto($x, $y),
                ];
    
    print json_encode($resposta);

?>